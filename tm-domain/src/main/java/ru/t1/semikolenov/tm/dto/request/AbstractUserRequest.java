package ru.t1.semikolenov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserRequest extends AbstractRequest {

    @Nullable
    protected String userId;
    protected String token;

    public AbstractUserRequest(@Nullable final String token) {
        this.token = token;
    }

}