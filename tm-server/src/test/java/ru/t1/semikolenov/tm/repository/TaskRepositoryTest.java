package ru.t1.semikolenov.tm.repository;

public class TaskRepositoryTest {

//    @NotNull
//    private final ITaskRepository taskRepository = new TaskRepository();
//
//    @NotNull
//    private static final String USER_ID_TEST_1 = UUID.randomUUID().toString();
//
//    @NotNull
//    private static final String USER_ID_TEST_2 = UUID.randomUUID().toString();
//
//    private static long INITIAL_SIZE;
//
//    @Before
//    public void init() {
//        taskRepository.create(USER_ID_TEST_1, "test_1");
//        taskRepository.create(USER_ID_TEST_1, "test_2");
//        taskRepository.create(USER_ID_TEST_2, "test_3");
//        taskRepository.create(USER_ID_TEST_2, "test_4");
//        taskRepository.create(USER_ID_TEST_2, "test_5");
//        INITIAL_SIZE = taskRepository.getSize();
//    }
//
//    @Test
//    public void create() {
//        taskRepository.create(USER_ID_TEST_1, "test_6");
//        Assert.assertEquals(INITIAL_SIZE + 1, taskRepository.getSize());
//        taskRepository.create(USER_ID_TEST_1, "test_7", "description");
//        Assert.assertEquals(INITIAL_SIZE + 2, taskRepository.getSize());
//    }
//
//    @Test
//    public void add() {
//        @NotNull final Task task = new Task();
//        taskRepository.add(USER_ID_TEST_1, task);
//        Assert.assertEquals(INITIAL_SIZE + 1, taskRepository.getSize());
//        Assert.assertTrue(taskRepository.findAll().contains(task));
//    }
//
//    @Test
//    public void clear() {
//        taskRepository.clear();
//        Assert.assertEquals(0, taskRepository.getSize());
//    }
//
//    @Test
//    public void findAll() {
//        @NotNull final List<Task> tasksAll = taskRepository.findAll();
//        Assert.assertEquals(INITIAL_SIZE, tasksAll.size());
//        @NotNull final List<Task> tasksUser1 = taskRepository.findAll(USER_ID_TEST_1);
//        Assert.assertEquals(2, tasksUser1.size());
//        @NotNull final List<Task> tasksUserEmpty = taskRepository.findAll(UUID.randomUUID().toString());
//        Assert.assertEquals(0, tasksUserEmpty.size());
//    }
//
//    @Test
//    public void findOneById() {
//        @NotNull final String taskName = "test_find_id";
//        @NotNull final Task task = taskRepository.create(USER_ID_TEST_1, taskName);
//        @NotNull final String taskId = task.getId();
//        Assert.assertNotNull(taskRepository.findOneById(taskId));
//        Assert.assertEquals(taskName, taskRepository.findOneById(taskId).getName());
//        Assert.assertNotNull(taskRepository.findOneById(USER_ID_TEST_1, taskId));
//        Assert.assertEquals(taskName, taskRepository.findOneById(USER_ID_TEST_1, taskId).getName());
//    }
//
//    @Test
//    public void findOneByIndex() {
//        @NotNull final String taskName = "test_find_index";
//        taskRepository.create(USER_ID_TEST_2, taskName);
//        @NotNull final Integer taskIndex = 3;
//        Assert.assertNotNull(taskRepository.findOneByIndex(USER_ID_TEST_2, taskIndex));
//        Assert.assertEquals(taskName, taskRepository.findOneByIndex(USER_ID_TEST_2, taskIndex).getName());
//    }
//
//    @Test
//    public void existsById() {
//        @NotNull final String taskName = "test_exist_id";
//        @NotNull final Task task = taskRepository.create(USER_ID_TEST_1, taskName);
//        @NotNull final String taskId = task.getId();
//        Assert.assertTrue(taskRepository.existsById(taskId));
//    }
//
//    @Test
//    public void remove() {
//        @NotNull final String taskName = "test_remove";
//        @NotNull final Task task = taskRepository.create(USER_ID_TEST_1, taskName);
//        @NotNull final String taskId = task.getId();
//        Assert.assertEquals(INITIAL_SIZE + 1, taskRepository.getSize());
//        taskRepository.remove(task);
//        Assert.assertNull(taskRepository.findOneById(taskId));
//        Assert.assertEquals(INITIAL_SIZE, taskRepository.getSize());
//        taskRepository.add(task);
//        Assert.assertEquals(INITIAL_SIZE + 1, taskRepository.getSize());
//        taskRepository.remove(USER_ID_TEST_1, task);
//        Assert.assertNull(taskRepository.findOneById(USER_ID_TEST_1, taskId));
//        Assert.assertEquals(INITIAL_SIZE, taskRepository.getSize());
//    }
//
//    @Test
//    public void removeById() {
//        @NotNull final Task task = taskRepository.create(USER_ID_TEST_1, "test_remove_id");
//        @NotNull final String taskId = task.getId();
//        Assert.assertEquals(INITIAL_SIZE + 1, taskRepository.getSize());
//        taskRepository.removeById(taskId);
//        Assert.assertNull(taskRepository.findOneById(taskId));
//        Assert.assertEquals(INITIAL_SIZE, taskRepository.getSize());
//        taskRepository.add(task);
//        Assert.assertEquals(INITIAL_SIZE + 1, taskRepository.getSize());
//        taskRepository.removeById(USER_ID_TEST_1, taskId);
//        Assert.assertNull(taskRepository.findOneById(USER_ID_TEST_1, taskId));
//        Assert.assertEquals(INITIAL_SIZE, taskRepository.getSize());
//    }
//
//    @Test
//    public void removeByIndex() {
//        taskRepository.create(USER_ID_TEST_2, "test_remove_index");
//        @NotNull final Integer taskIndex = 3;
//        taskRepository.removeByIndex(USER_ID_TEST_2, taskIndex);
//        Assert.assertThrows(IndexOutOfBoundsException.class,
//                () -> taskRepository.findOneByIndex(USER_ID_TEST_2, taskIndex));
//        Assert.assertEquals(INITIAL_SIZE, taskRepository.getSize());
//    }

}
