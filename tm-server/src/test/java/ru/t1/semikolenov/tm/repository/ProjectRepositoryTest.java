package ru.t1.semikolenov.tm.repository;

public class ProjectRepositoryTest {

//    @NotNull
//    private final IProjectRepository projectRepository = new ProjectRepository();
//
//    @NotNull
//    private static final String USER_ID_TEST_1 = UUID.randomUUID().toString();
//
//    @NotNull
//    private static final String USER_ID_TEST_2 = UUID.randomUUID().toString();
//
//    private static long INITIAL_SIZE;
//
//    @Before
//    public void init() {
//        projectRepository.create(USER_ID_TEST_1, "test_1");
//        projectRepository.create(USER_ID_TEST_1, "test_2");
//        projectRepository.create(USER_ID_TEST_2, "test_3");
//        projectRepository.create(USER_ID_TEST_2, "test_4");
//        projectRepository.create(USER_ID_TEST_2, "test_5");
//        INITIAL_SIZE = projectRepository.getSize();
//    }
//
//    @Test
//    public void create() {
//        projectRepository.create(USER_ID_TEST_1, "test");
//        Assert.assertEquals(INITIAL_SIZE + 1, projectRepository.getSize());
//        projectRepository.create(USER_ID_TEST_1, "test", "description");
//        Assert.assertEquals(INITIAL_SIZE + 2, projectRepository.getSize());
//    }
//
//    @Test
//    public void add() {
//        @NotNull final Project project = new Project();
//        projectRepository.add(USER_ID_TEST_1, project);
//        Assert.assertEquals(INITIAL_SIZE + 1, projectRepository.getSize());
//        Assert.assertTrue(projectRepository.findAll().contains(project));
//    }
//
//    @Test
//    public void clear() {
//        projectRepository.clear();
//        Assert.assertEquals(0, projectRepository.getSize());
//    }
//
//    @Test
//    public void findAll() {
//        @NotNull final List<Project> projectsAll = projectRepository.findAll();
//        Assert.assertEquals(INITIAL_SIZE, projectsAll.size());
//        @NotNull final List<Project> projectsUser1 = projectRepository.findAll(USER_ID_TEST_1);
//        Assert.assertEquals(2, projectsUser1.size());
//        @NotNull final List<Project> projectsUserEmpty = projectRepository.findAll(UUID.randomUUID().toString());
//        Assert.assertEquals(0, projectsUserEmpty.size());
//    }
//
//    @Test
//    public void findOneById() {
//        @NotNull final String projectName = "test_find_id";
//        @NotNull final Project project = projectRepository.create(USER_ID_TEST_1, projectName);
//        @NotNull final String projectId = project.getId();
//        Assert.assertNotNull(projectRepository.findOneById(projectId));
//        Assert.assertEquals(projectName, projectRepository.findOneById(projectId).getName());
//        Assert.assertNotNull(projectRepository.findOneById(USER_ID_TEST_1, projectId));
//        Assert.assertEquals(projectName, projectRepository.findOneById(USER_ID_TEST_1, projectId).getName());
//    }
//
//    @Test
//    public void findOneByIndex() {
//        @NotNull final String projectName = "test_find_index";
//        projectRepository.create(USER_ID_TEST_2, projectName);
//        @NotNull final Integer projectIndex = 3;
//        Assert.assertNotNull(projectRepository.findOneByIndex(USER_ID_TEST_2, projectIndex));
//        Assert.assertEquals(projectName, projectRepository.findOneByIndex(USER_ID_TEST_2, projectIndex).getName());
//    }
//
//    @Test
//    public void existsById() {
//        @NotNull final String projectName = "test_exist_id";
//        @NotNull final Project project = projectRepository.create(USER_ID_TEST_1, projectName);
//        @NotNull final String projectId = project.getId();
//        Assert.assertTrue(projectRepository.existsById(projectId));
//    }
//
//    @Test
//    public void remove() {
//        @NotNull final String projectName = "test_remove";
//        @NotNull final Project project = projectRepository.create(USER_ID_TEST_1, projectName);
//        @NotNull final String projectId = project.getId();
//        Assert.assertEquals(INITIAL_SIZE + 1, projectRepository.getSize());
//        projectRepository.remove(project);
//        Assert.assertNull(projectRepository.findOneById(projectId));
//        Assert.assertEquals(INITIAL_SIZE, projectRepository.getSize());
//        projectRepository.add(project);
//        Assert.assertEquals(INITIAL_SIZE + 1, projectRepository.getSize());
//        projectRepository.remove(USER_ID_TEST_1, project);
//        Assert.assertNull(projectRepository.findOneById(USER_ID_TEST_1, projectId));
//        Assert.assertEquals(INITIAL_SIZE, projectRepository.getSize());
//    }
//
//    @Test
//    public void removeById() {
//        @NotNull final Project project = projectRepository.create(USER_ID_TEST_1, "test_remove_id");
//        @NotNull final String projectId = project.getId();
//        Assert.assertEquals(INITIAL_SIZE + 1, projectRepository.getSize());
//        projectRepository.removeById(projectId);
//        Assert.assertNull(projectRepository.findOneById(projectId));
//        Assert.assertEquals(INITIAL_SIZE, projectRepository.getSize());
//        projectRepository.add(project);
//        Assert.assertEquals(INITIAL_SIZE + 1, projectRepository.getSize());
//        projectRepository.removeById(USER_ID_TEST_1, projectId);
//        Assert.assertNull(projectRepository.findOneById(USER_ID_TEST_1, projectId));
//        Assert.assertEquals(INITIAL_SIZE, projectRepository.getSize());
//    }
//
//    @Test
//    public void removeByIndex() {
//        projectRepository.create(USER_ID_TEST_2, "test_remove_index");
//        @NotNull final Integer projectIndex = 3;
//        Assert.assertEquals(INITIAL_SIZE + 1, projectRepository.getSize());
//        projectRepository.removeByIndex(USER_ID_TEST_2, projectIndex);
//        Assert.assertThrows(IndexOutOfBoundsException.class,
//                () -> projectRepository.findOneByIndex(USER_ID_TEST_2, projectIndex));
//        Assert.assertEquals(INITIAL_SIZE, projectRepository.getSize());
//    }

}
